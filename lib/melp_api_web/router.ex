defmodule MelpApiWeb.Router do
  use MelpApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  # The following scope will contain all CRUD operations for Restaruant table/model
  scope "/api/v1", MelpApiWeb do
    pipe_through :api
    # This line generates all endpoints for CRUD operations 
    # resources "/restaurants", RestaurantController

    # i create one by one manually
    get "/restaurants", RestaurantController, :index
    get "/restaurants/:id", RestaurantController, :show
    post "/restaurants", RestaurantController, :create
    patch "/restaurants/:id", RestaurantController, :update
    delete "/restaurants/:id", RestaurantController, :delete
    

  end

  scope "/api/swagger" do
    forward "/", PhoenixSwagger.Plug.SwaggerUI,
      otp_app: :melp_api,
      swagger_file: "swagger.json"
  end

  def swagger_info do
    %{
      basePath: "/api/v1",
      schemes: ["http", "https"],
      info: %{
        version: "1.0",
        title: "Melp API",
        description: "API Documentation for Melp API v1",
        contact: %{
          name: "David Valencia",
          email: "dvidvlencia@gmail.com"
        }
      },
      consumes: ["application/json"],
      produces: ["application/json"],
      tags: [
        %{name: "Restaurants", description: "Restaurant resources"}
      ]
    }
  end

end
