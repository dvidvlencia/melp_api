defmodule MelpApiWeb.RestaurantController do
  use MelpApiWeb, :controller
  import Plug.Conn.Status, only: [code: 1]
  use PhoenixSwagger

  alias MelpApi.Restaurants
  alias MelpApi.Restaurants.Restaurant

  action_fallback MelpApiWeb.FallbackController

  # Definition of schemas in order to be used in "swagger_path" 
  def swagger_definitions do
    %{
      Restaurant:
        swagger_schema do
          title("Restaurants")
          description("A restaurant of the application")
  
          properties do
            name(:string, "Restaurant name", required: true)
            city(:string, "Restaurant attribute", required: true)
            email(:string, "Restaurant attribute", required: true)
            phone(:string, "Restaurant attribute", required: true)
            rating(:integer, "Restaurant attribute", required: true)
            site(:string, "Restaurant attribute", required: true)
            state(:string, "Restaurant attribute", required: true)
            street(:string, "Restaurant attribute", required: true)
            lng(:float, "Restaurant attribute", required: true)
            lat(:float, "Restaurant attribute", required: true)
          end
        end
    }
  end

  swagger_path :index do
    get("/restaurants")
    description("This endpoint returns a list of restaurants")
    produces "application/json"
    tag "Restaurants"
    response 200, "Success", :Restaurant, examples: %{
      "application/json": %{
        "data": [
          %{
            "city": "cdmx",
            "email": "quierocomer@tacorriendo.mx",
            "id": "65b414c0-7885-4dcf-bfd6-b68ac97b835b",
            "lat": 19.4426838205224,
            "lng": -99.1250245928884,
            "name": "TaCorriendo -Tacos de barbacoa",
            "phone": "554925658",
            "rating": 3,
            "site": "http://tacorriendo.mx",
            "state": "cdmx",
            "street": "Av Paseo de la reforma"
          }
        ]
      }
    }
  end
  def index(conn, _params) do
    restaurants = Restaurants.list_restaurants()
    render(conn, "index.json", restaurants: restaurants)
  end

  swagger_path :create do
    post("/restaurants")
    description("This endpoint receives a map/json and create a new restaurant")
    tag "Restaurants"
    parameters do
      restaurant :body, Schema.ref(:Restaurant), "Restaurant attributes"
    end
    response 200, "Success", :Restaurant, examples: %{
      "application/json": %{
        "data": %{
            "city": "cdmx",
            "email": "quierocomer@tacorriendo.mx",
            "id": "65b414c0-7885-4dcf-bfd6-b68ac97b835b",
            "lat": 19.4426838205224,
            "lng": -99.1250245928884,
            "name": "TaCorriendo -Tacos de barbacoa",
            "phone": "554925658",
            "rating": 3,
            "site": "http://tacorriendo.mx",
            "state": "cdmx",
            "street": "Av Paseo de la reforma"
      }
    }
  }
  end
  def create(conn, %{"restaurant" => restaurant_params}) do
    with {:ok, %Restaurant{} = restaurant} <- Restaurants.create_restaurant(restaurant_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.restaurant_path(conn, :show, restaurant))
      |> render("show.json", restaurant: restaurant)
    end
  end

  swagger_path :show do
    get("/restaurants/:id")
    description("This endpoint returns a restaurant")
    produces "application/json"
    tag "Restaurants"
    parameter :id, :query, :text, "restaurant uuid", required: true
    response 200, "Success", :Restaurant, examples: %{
      "application/json": %{
        "data": %{
            "city": "cdmx",
            "email": "quierocomer@tacorriendo.mx",
            "id": "65b414c0-7885-4dcf-bfd6-b68ac97b835b",
            "lat": 19.4426838205224,
            "lng": -99.1250245928884,
            "name": "TaCorriendo -Tacos de barbacoa",
            "phone": "554925658",
            "rating": 3,
            "site": "http://tacorriendo.mx",
            "state": "cdmx",
            "street": "Av Paseo de la reforma"
          }
      }
    }
  end
  def show(conn, %{"id" => id}) do
    restaurant = Restaurants.get_restaurant!(id)
    render(conn, "show.json", restaurant: restaurant)
  end

  swagger_path :update do
    patch("/restaurants/:id")
    description("This endpoint receives a map/json and update an existing restaurant")
    tag "Restaurants"
    parameters do
      id :path, :string, "Restaurant UUID"
      restaurant :body, Schema.ref(:Restaurant), "Restaurant attributes"
    end
    response 200, "Success", :Restaurant, examples: %{
      "application/json": %{
        "data": %{
            "city": "cdmx",
            "email": "quierocomer@tacorriendo.mx",
            "id": "65b414c0-7885-4dcf-bfd6-b68ac97b835b",
            "lat": 19.4426838205224,
            "lng": -99.1250245928884,
            "name": "TaCorriendo -Tacos de barbacoa",
            "phone": "554925658",
            "rating": 3,
            "site": "http://tacorriendo.mx",
            "state": "cdmx",
            "street": "Av Paseo de la reforma"
      }
    }
  }
  end
  def update(conn, %{"id" => id, "restaurant" => restaurant_params}) do
    restaurant = Restaurants.get_restaurant!(id)

    with {:ok, %Restaurant{} = restaurant} <- Restaurants.update_restaurant(restaurant, restaurant_params) do
      render(conn, "show.json", restaurant: restaurant)
    end
  end
  
  swagger_path :delete do
    PhoenixSwagger.Path.delete("/restaurants/:id")
    description("This endpoint deletes a restaurant by UUID, returns an empty body")
    produces "application/json"
    tag "Restaurants"
    parameter :id, :path, :text, "restaurant uuid", required: true, example: "65b414c0-7885-4dcf-bfd6-b68ac97b835b"
    response 203, "No Content - Deleted Successfully"
  end
  def delete(conn, %{"id" => id}) do
    restaurant = Restaurants.get_restaurant!(id)

    with {:ok, %Restaurant{}} <- Restaurants.delete_restaurant(restaurant) do
      send_resp(conn, :no_content, "")
    end
  end
end
