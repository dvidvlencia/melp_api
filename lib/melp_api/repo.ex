defmodule MelpApi.Repo do
  use Ecto.Repo,
    otp_app: :melp_api,
    adapter: Ecto.Adapters.Postgres
end
