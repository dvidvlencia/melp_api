# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :melp_api,
  ecto_repos: [MelpApi.Repo]

# Configures the endpoint
config :melp_api, MelpApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "OxrOX2PBc/HlGy3RJTP/L4rvG84NW1/WjvBgjFsn0Z2AwVd7YA/OuedLH2YDjQD2",
  render_errors: [view: MelpApiWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: MelpApi.PubSub,
  live_view: [signing_salt: "3Z53FWlG"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :melp_api, :phoenix_swagger,
  swagger_files: %{
    "priv/static/swagger.json" => [
      router: MelpApiWeb.Router,     # phoenix routes will be converted to swagger paths
      endpoint: MelpApiWeb.Endpoint  # (optional) endpoint config used to set host, port and https schemes.
    ]
  }

config :phoenix_swagger, json_library: Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
