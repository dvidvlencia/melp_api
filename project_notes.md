# Create project
first i need create a project wich exclude some front-end features
> mix phx.new melp_api --no-html --no-webpack --no-dashboard

After create a project is necessary add a .env file in order to avoid track psql credentials then read the enviroment variables in dev.ex

when database credentials are ready, we need define a context where the application wrap the logic.
in order to generate automatically the context, model, controller, migration, etc... i use the next command
> mix phx.gen.json Restaurants Restaurant restaurants rating:integer name:text site:text email:text phone:text street:text city:text state:text lat:float lng:float --table restaurants
it was necessary change the default type in this table (migration), i use "primary_key: true" and type "uuid" in order to store correctly the raw data from csv file

The previous command creates a file called restaurants.ex which only contain basic queries in order to save, update, delete or show info from our dattabase. That funcionts executes Repo callbacks 


# deploy
Create project
> heroku create <APP_NAME> --buildpack hashnuke/elixir

add buildpacks

> heroku buildpacks:add https://github.com/gjaldon/heroku-buildpack-phoenix-static.git


Add database plugin
> heroku addons:create heroku-postgresql:hobby-dev

Add pool size
> heroku config:set POOL_SIZE=18

"""
Created postgresql-closed-18301 as DATABASE_URL
Use heroku addons:docs heroku-postgresql to view documentation
"""
Add secret key
> heroku config:set SECRET_KEY_BASE="<STR>"

add heroku remote
> heroku git:remote -a <APP_NAME>

load migrations
> heroku run "POOL_SIZE=2 mix ecto.migrate"

Final note: I added direct commits in master and had to move them to a hotfix branch using git cherry-pick,
those changes fix the deploy process in heroku